package main

import (
	"fmt"
	"gitlab.com/thomasv/custom-protocol-parser/pkg/mps7"
	"log"
	"os"
)

func main() {
	path := "./txnlog.dat"

	data, err := mps7.DataSetFromFile(path)

	if err != nil {
		log.Fatalln("Error parsing MPS7 file", err)
		os.Exit(1)
	}

	fmt.Println("Q: What is the total amount in dollars of debits?")
	fmt.Printf("A: $%f\n", data.SumAmountByType(mps7.Debit))

	fmt.Println("Q: What is the total amount in dollars of credits?")
	fmt.Printf("A: $%f\n", data.SumAmountByType(mps7.Credit))

	fmt.Println("Q: How many autopays were started?")
	fmt.Printf("A: %d\n", len(data.RecordsByType(mps7.StartAutopay)))

	fmt.Println("Q: How many autopays were ended?")
	fmt.Printf("A: %d\n", len(data.RecordsByType(mps7.EndAutopay)))

	fmt.Println("Q: How many credits were there?")
	fmt.Printf("A: %d\n", len(data.RecordsByType(mps7.Credit)))

	fmt.Println("Q: How many autopays were ended?")
	fmt.Printf("A: %d\n", len(data.RecordsByType(mps7.Debit)))

	fmt.Println("Q: What is balance of user ID?")
	fmt.Printf("A: %f\n", data.BalanceByUserId(2456938384156277127))
}
