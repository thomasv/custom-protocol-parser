package mps7

type RecordType byte

const (
	Debit RecordType = iota
	Credit
	StartAutopay
	EndAutopay
)

// Constants related to binary format positions and sizes
const (
	HeaderByteSize            int = 9
	HeaderMagicStringBytePos  int = 0
	HeaderMagicStringByteSize int = 4
	HeaderVersionBytePos      int = 4
	HeaderRecordCountBytePos  int = 5
	RecordByteSize            int = 13
	RecordTimestampBytePos    int = 1
	RecordTimestampByteLength int = 4
	RecordUserIdBytePos       int = 5
	RecordAmountByteSize      int = 8
)

type DataSet struct {
	Header  Header
	Records []Record
}

type Header struct {
	Version    byte
	NumRecords uint32
}

type Record struct {
	Type      RecordType
	Timestamp uint32
	UserId    uint64
	Amount    float64
}

func (d DataSet) SumAmountByType(t RecordType) (amount float64) {
	records := d.RecordsByType(t)
	for _, record := range records {
		amount = amount + record.Amount
	}

	return amount
}

func (d DataSet) RecordsByType(t RecordType) (recordsByType []Record) {
	for i, _ := range d.Records {
		if d.Records[i].Type == t {
			recordsByType = append(recordsByType, d.Records[i])
		}
	}

	return recordsByType
}

func (d DataSet) BalanceByUserId(uid uint64) (amount float64) {
	records := d.RecordsByUserId(uid)

	for i, _ := range records {
		if records[i].Type == Debit {
			amount = amount - records[i].Amount
		} else if records[i].Type == Credit {
			amount = amount + records[i].Amount
		}
	}

	return
}

func (d DataSet) RecordsByUserId(uid uint64) (recordsByUid []Record) {
	for i, _ := range d.Records {
		if d.Records[i].UserId == uid {
			recordsByUid = append(recordsByUid, d.Records[i])
		}
	}

	return recordsByUid
}
