package mps7

import (
	"testing"
)

func TestRecordByType(t *testing.T) {
	ds, err := DataSetFromFile("../../txnlog.dat")
	if err != nil {
		t.Fatal("got an error reading dataset from file.")
	}

	filteredRecords := ds.RecordsByType(Debit)

	if len(filteredRecords) == len(ds.Records) {
		t.Fatal("filteredRecords and length of total records is the same")
	}

	if len(filteredRecords) == 0 {
		t.Fatal("filteredRecords length is zero")
	}

	isNotDebit := false

	for _, record := range filteredRecords {
		if record.Type != Debit {
			isNotDebit = true
		}
	}

	if isNotDebit {
		t.Fatal("Found a non-Debit type record in filter results")
	}
}

func TestParser(t *testing.T) {
	ds, err := DataSetFromFile("../../txnlog.dat")
	if err != nil {
		t.Fatal("got an error reading dataset from file.")
	}

	firstRecord := ds.Records[0]

	if firstRecord.Type != Debit {
		t.Fatal("First record is not a Debit type")
	}

	if firstRecord.Timestamp != 1393108945 {
		t.Fatal("First record does not have the correct Timestamp", firstRecord.Timestamp)
	}

	if firstRecord.UserId != 4136353673894269217 {
		t.Fatal("First record does not have the correct UserID", firstRecord.UserId)
	}

	if firstRecord.Amount != 604.274335557087 {
		t.Fatal("First record does not have the correct Amount", firstRecord.Amount)
	}
}

func TestSumRecordsByType(t *testing.T) {
	ds, err := DataSetFromFile("../../txnlog.dat")
	if err != nil {
		t.Fatal("got an error reading dataset from file.")
	}

	expectedDebitAmount := 18203.69953340208
	actualDebitAmount := ds.SumAmountByType(Debit)

	if expectedDebitAmount != actualDebitAmount {
		t.Fatal("expected and actual debit amounts differ")
	}
}
