package mps7

import (
	"encoding/binary"
	"errors"
	"io"
	"log"
	"math"
	"os"
)

func DataSetFromFile(pathName string) (data DataSet, err error) {
	file, err := os.Open(pathName)
	if err != nil {
		log.Fatalf("Error opening file %s", err)
	}
	defer file.Close()

	data, err = ParseFile(file)
	return
}

func ParseFile(file *os.File) (data DataSet, err error) {
	header, err := parseHeader(file)
	if err != nil {
		return
	}

	data.Header = header

	records, err := parseRecords(file)
	if err != nil && err != io.EOF {
		return
	}

	data.Records = records

	return data, nil
}

func parseRecords(file *os.File) (records []Record, err error) {
	record, err := parseRecord(file)
	for err != io.EOF {
		records = append(records, record)
		record, err = parseRecord(file)
	}

	return
}

func parseRecord(file *os.File) (record Record, err error) {
	recordStart, err := getNextBytes(file, RecordByteSize)
	if err != nil {
		return
	}

	record.Type = RecordType(recordStart[0])

	recordTimestampBytes :=
		recordStart[RecordTimestampBytePos : RecordTimestampBytePos+RecordTimestampByteLength]

	record.Timestamp = binary.BigEndian.Uint32(recordTimestampBytes)

	recordUserIdBytes := recordStart[RecordUserIdBytePos:]
	record.UserId = binary.BigEndian.Uint64(recordUserIdBytes)

	if record.Type == Debit || record.Type == Credit {
		var recordAmountBytes []byte
		recordAmountBytes, err = getNextBytes(file, RecordAmountByteSize)
		if err != nil {
			return
		}
		recordAmountBits := binary.BigEndian.Uint64(recordAmountBytes)
		record.Amount = math.Float64frombits(recordAmountBits)
	}

	return
}

func parseHeader(file *os.File) (header Header, err error) {
	headerBytes, err := getNextBytes(file, HeaderByteSize)
	if err != nil {
		return
	}

	magicHeaderString :=
		string(headerBytes[HeaderMagicStringBytePos:HeaderMagicStringByteSize])

	if magicHeaderString != "MPS7" {
		err = errors.New("File is not MPS7 format.")
		return
	}

	header.Version = headerBytes[HeaderVersionBytePos]
	header.NumRecords = binary.BigEndian.Uint32(headerBytes[HeaderRecordCountBytePos:])

	return
}

func getNextBytes(file *os.File, number int) (bytes []byte, err error) {
	bytes = make([]byte, number)
	_, err = file.Read(bytes)
	return
}
