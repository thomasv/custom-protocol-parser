#!/bin/bash
GOOS=linux GOARCH=amd64 go build -o custom-proto-parser-linux-amd64 main.go
GOOS=darwin GOARCH=amd64 go build -o custom-proto-parser-darwin-amd64 main.go
