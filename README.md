# Custom Protocol Format Parser

Implementation of the challenge located here: https://github.com/adhocteam/homework/tree/master/proto

# Answers

Answers can be found by executing the binary or running `go run main.go`

# Development
```
mkdir -p $GOPATH/src/github.com/thomasv314/ && cd $GOPATH/src/github.com/thomasv314
git clone git@gitlab.com:thomasv/custom-protocol-parser.git
go get
go run main.go
```
